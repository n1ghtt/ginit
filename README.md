#ginit

## Initialise a current directory and link it to the remote repository

#Why?

**Simple!**

Because when you want to set up your current directory as git and link it to a remote respository you would have to go through 5 commands, but with my script you just have to provide a link to your repository and it will do a job for you.

After you have done that you can use `gq` command to make further commits, this will speed up your git workflow drastically, and you dont have to worry if you forgot the command sequence.

**NOTE** It is currently underdevelopment, improvements are to come

* * *

##TO-DO

+ add link validation before executing,
+ add useful error messages
+ try 3 times before exiting, or type q to quit
+ .......... more more more ..........


Link to [gq](https://bitbucket.org/n1ghtt/git_quickadd-commit-push) command