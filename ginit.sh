#!/bin/bash
echo "Please specify a link to a remote repository!"
read link
if [[ $link != "" ]]; then
	git init
	git add .
	git commit -m "Initial Commit"
	git remote add origin $link
	git push origin master
else
	echo "You need to specify a proper link to a remote repository!"
	echo "Some that will look like this https://bitbucket.org/username/name_of_your_repository "
fi